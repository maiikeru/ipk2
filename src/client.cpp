

#include <iostream>
#include <regex.h>
#include <cstdlib>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

// nemusi se pouzivat std::
using namespace std;

typedef struct
{
    string source;
    string host;
    string port_s;
    string file;
    long port;
} data;


bool GetNum(data *point)
{
    bool retCode = true;
    char * pEnd;
    long  num = strtol (point->port_s.c_str(), &pEnd, 10);
    
    if (strcmp(pEnd, "\0") != 0)
    {
        retCode = false;
        num = -1;
        cerr << "Error, invalid input:" << point->port_s.c_str() << "." <<endl; 
    }       

    point->port = num;
    return retCode;
}


bool GetPort(data *point)
{

    bool retCode = true;
    if (GetNum(point))
    {
        if (point->port <= 0 || point->port > 65535)
        {
            cerr << "Error, invalid port range. Try <1,65535>." << endl;
            retCode = false;
        }
    }
    else
        retCode = false;
    
    return retCode;
}

bool ParseInput(data *point)
{

    regex_t start_state;
    const char *pattern = "(.+)\\:([0-9]+)\\/(.+)";
    int status;
    char *dup;
    if (0 != (status = regcomp(&start_state, pattern, REG_EXTENDED)))
    {
        cerr << "chyba pri regulernim vyrazu" << endl;
        exit(EXIT_FAILURE);
    }

    if (0 == (status = regexec(&start_state, point->source.c_str(), (size_t) 0, NULL, 0)))
    {
        // ziskani hosta
        dup = strdup(point->source.c_str());
        point->host = strtok(dup, ":");
        point->source.erase(0, point->host.length() + 1);

        // ziskani portu
        dup = strdup(point->source.c_str());
        point->port_s = strtok(dup, "/");
        point->source.erase(0, point->port_s.length() + 1);

        // zavolani funkce na prevod stringu na cislo
        if (!GetPort(point))
            return false;

        // jmeno souboru
        point->file = point->source;
    }
    else
    {
        cerr << "Error, invalid input argument" << endl;
        cerr << "Usage: host:port/soubor" <<endl;
        return false;
    }

    return true;
}

void *getAdress(struct sockaddr *sa)
{
    return &(((struct sockaddr_in*)sa)->sin_addr);
}


int main(int argc, char const *argv[])
{
    // inicializace struktury
    data pointer;

    //cout << "Client is starting" <<endl;

    if (argc != 2)
    {
        cerr << "Eror, undefined arguments" << endl;
        cerr << "Usage: '" << argv[0] << " host:port/soubor'." <<endl;
        return 0;
    }

    // převedení vstupu na string
    int i = 0;
    while (argv[1][i])
    {
        pointer.source += argv[1][i];
        i++;
    }

    if(!ParseInput(&pointer))
        return 0;

    int sockfd, numbytes;  
    char buf[3];
    struct addrinfo hints, *servinfo, *pont;
    int rv;
    char s[INET6_ADDRSTRLEN];


    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo(pointer.host.c_str(), pointer.port_s.c_str(), &hints, &servinfo)) != 0) {
        cerr << "error getaddrinfo:" << gai_strerror(rv);
        return 1;
    }

    for(pont = servinfo; pont != NULL; pont = pont->ai_next) {
        if ((sockfd = socket(pont->ai_family, pont->ai_socktype,
                pont->ai_protocol)) == -1) {
            cerr << "error cretw socket" <<endl;
            continue;
        }

        if (connect(sockfd, pont->ai_addr, pont->ai_addrlen) == -1) {
            cerr << "error connect" <<endl;
            close(sockfd);
            continue;
        }

        break;
    }

    if (pont == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }


    inet_ntop(pont->ai_family, getAdress((struct sockaddr *)pont->ai_addr),
            s, sizeof s);

    freeaddrinfo(servinfo); 


    if (send(sockfd, pointer.file.c_str() , strlen(pointer.file.c_str()), 0) == -1)
        perror("send");


    if ((numbytes = recv(sockfd, buf, 1, 0)) == -1) {
        perror("recv");
        exit(1);
    }

    buf[numbytes] = '\0';

    int block_size;
    char buffer[1000];

    if (strcmp(buf, "0") == 0)
    {
        cerr << "File dont exist" <<endl;
        return 0;
    }
    

    FILE * pFile;
    if ((pFile = fopen (pointer.file.c_str(),"w")) == NULL)
    {
        
        cout << "Error:create file" <<endl;
        return 0;
    }

     while(block_size = recv(sockfd, buffer, 1000, 0))
        {
            if(block_size == 0)
            {
                break;
            }
            int write_size = fwrite(buffer, sizeof(char), block_size, pFile);
            if(write_size < block_size)
            {
                perror("File write failed.\n");
            }

            if(block_size < 0)
            {
                perror("Receive file error.\n");
            }
            
        }

    close(sockfd);


    return 0;
}
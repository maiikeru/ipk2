

#include <iostream>
#include <string.h>
#include <string>

#include <cstdlib>
#include <cerrno>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>


// nemusi se pouzivat std::
using namespace std;


bool GetNum(const char *src, long *number)
{
    bool retCode = true;
    char *pEnd;
    long  num = strtol (src, &pEnd, 10);

    if (strcmp(pEnd, "\0") != 0)
    {
        retCode = false;
        num = -1;
        cerr << "Error, invalid input:" << src << "." << endl;
    }

    *number = num;
    return retCode;
}


bool GetPort(const char *src, long  *port)
{

    bool retCode = true;
    if (GetNum(src, port))
    {
        if (*port <= 0 || *port > 65535)
        {
            cerr << "Error, invalid port range. Try <1,65535>." << endl;
            retCode = false;
        }
    }
    else
        retCode = false;

    return retCode;
}

bool GetSpeed(const char *src, long  *speed)
{

    if (GetNum(src, speed))
    {
        if (*speed <= 0)
        {
            cerr << "Error, invalid input of speed '" << *speed << "̈́'." << endl;
            return false;
        }
    }
    else
        return false;

    return true;
}


void sigchld_handler(int s)
{
    while (waitpid(-1, NULL, WNOHANG) > 0);
}

void *getAdress(struct sockaddr *sa)
{
        return &(((struct sockaddr_in *)sa)->sin_addr);
}

int main(int argc, char const *argv[])
{
    cout << "Server is starting" << endl;

    long  port = 0;
    long  speed = 0;
    const char *port_s;

    if (argc != 5)
    {
        cerr << "Eror, undefined arguments" << endl;
        return 0;
    }
    else
    {
        if ((strcmp(argv[1], "-p") == 0) && (strcmp(argv[3], "-d") == 0))
        {
            if (!GetPort(argv[2], &port) || !GetSpeed(argv[4], &speed))
                return -1;

            port_s = argv[2];
        }
        else if ((strcmp(argv[3], "-p") == 0) && (strcmp(argv[1], "-d") == 0))
        {
            if (!GetPort(argv[4], &port) || !GetSpeed(argv[2], &speed))
                return -1;

            port_s = argv[4];
        }
        else
        {
            cerr << "Eror, undefined arguments" << endl;
            return 0;
        }
    }


    int sockfd, new_sock;  
    struct addrinfo sinn, *servinfo, *pont;
    struct sigaction sig;
    struct sockaddr_storage address; 
    socklen_t sinn_size;
    char s[INET6_ADDRSTRLEN];

    memset(&sinn, 0, sizeof sinn);
    sinn.ai_family = AF_UNSPEC;
    sinn.ai_socktype = SOCK_STREAM;
    sinn.ai_flags = AI_PASSIVE; 

    int rv;
    if ((rv = getaddrinfo(NULL, port_s, &sinn, &servinfo)) != 0)
    {
        cerr << "error getaddrinfo:" << gai_strerror(rv);
        return 1;
    }

    int stat = 1;
    for (pont = servinfo; pont != NULL; pont = pont->ai_next)
    {
        if ((sockfd = socket(pont->ai_family, pont->ai_socktype,
                             pont->ai_protocol)) == -1)
        {
            cerr << "error cretw socket" <<endl;
            continue;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &stat,
                       sizeof(int)) == -1)
        {
            cerr <<"set socket" <<endl;
            exit(1);
        }

        if (bind(sockfd, pont->ai_addr, pont->ai_addrlen) == -1)
        {
            cerr << "error bind" <<endl;
            close(sockfd);
            continue;
        }

        break;
    }

    if (pont == NULL)
    {
        cerr << "error bind" <<endl;
        return 2;
    }

    freeaddrinfo(servinfo); 

    if (listen(sockfd, 5) == -1)
    {
        perror("listen");
        exit(1);
    }

    sig.sa_handler = sigchld_handler; 
    sigemptyset(&sig.sa_mask);
    sig.sa_flags = SA_RESTART;

    if (sigaction(SIGCHLD, &sig, NULL) == -1)
    {
        perror("sigaction");
        exit(1);
    }


    while (1)
    {
        sinn_size = sizeof address;
        new_sock = accept(sockfd, (struct sockaddr *)&address, &sinn_size);
        if (new_sock == -1)
        {
            perror("accept");
            continue;
        }

        inet_ntop(address.ss_family,
                  getAdress((struct sockaddr *)&address),
                  s, sizeof s);

        int numbytes;
        char buf[100];
        int block_size;
        char buffer[1000];

        if (!fork())
        {
            close(sockfd);

            if ((numbytes = recv(new_sock, buf, sizeof(buf), 0)) == -1)
            {
                perror("recv");
                exit(1);
            }

            buf[numbytes] = '\0';
            int count = 0;
            FILE *pFile;
            register double slep_num = (1 / speed) * 1000000;
            if ((pFile = fopen (buf, "r")) == NULL)
            {
                if (send(new_sock, "0", 1, 0) == -1)
                    perror("send");
            }
            else
            {
                if (send(new_sock, "1", 1, 0) == -1)
                    perror("send");

                while ((block_size = fread(buffer, sizeof(char), 1000, pFile)) > 0)
                {
                    if (send(new_sock, buffer, block_size, 0) < 0)
                    {
                        cerr << "ERROR: Failed to send file" <<endl;
                        exit(1);
                    }
                    usleep(slep_num);
                }
            }


            close(new_sock);
            exit(0);
        }
        close(new_sock);
    }

    return 0;
}

